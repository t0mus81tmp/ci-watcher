package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class ProjectsActivity extends AbstractNavigableActivity {

    public static final String PROJECT_KEY = "projectKey";
    public static final String FAV_PROJECTS = "fav-projects";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plans);
        ListView lv = (ListView) findViewById(R.id.listView);
        data = (ProjectsData) getLastCustomNonConfigurationInstance();
        if (data == null) {
            data = new ProjectsData();
            data.setAdapter(new ProjectAdapter(ProjectsActivity.this, new ArrayList<Project>(0), FavouritesUtil.isFavouriteMode(getIntent())));
            ((ProjectsData)data).setCount(0);
            data.setTimer(new Timer());
            data.setHandler(new Handler());
            data.setFetchers(new ArrayList<JSONFetcher>(SettingsActivity.DEFAULT_FETCHERS_COUNT));
            data.setQueue(new ArrayList<JSONFetcher>());
            data.setStartIndex(SettingsActivity.DEFAULT_START_INDEX);
            if (FavouritesUtil.isFavouriteMode(getIntent())) {
                data.setTitleString(getResources().getString(R.string.favourite) + " " + getResources().getString(R.string.projects));
            } else {
                data.setTitleString(getResources().getString(R.string.all) + " " + getResources().getString(R.string.projects));
            }
            data.setProgressDialog(null);
            data.setErrorDialog(null);
            ((ProjectsData)data).setAddToFavouritesDialog(null);
        } else {
            if(data.isErrorDialogDisplayed()){
                data.setErrorDialog(DialogsUtil.createErrorDialog(data.getErrorCode(), ProjectsActivity.this));
                data.getErrorDialog().show();
            }else if(((ProjectsData)data).isAddToFavouritesDialogDisplayed()){
                ((ProjectsData)data).setAddToFavouritesDialog(FavouritesUtil.createAddToFavouritesDialog(((ProjectsData)data).getFavouriteKey(), false, ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this));
                ((ProjectsData)data).getAddToFavouritesDialog().show();
            }else if(((ProjectsData)data).isRemoveFromFavouritesDialogDisplayed()){
                ((ProjectsData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(((ProjectsData)data).getFavouriteKey(), false, ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this));
                ((ProjectsData)data).getRemoveFromFavouritesDialog().show();
            }else{
                if(data.isProgressDialogDisplayed()){
                    data.setProgressDialog(ProgressDialog.show(ProjectsActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.projects)));
                }
            }
        }
        getSupportActionBar().setTitle(data.getTitleString());
        lv.setAdapter(data.getAdapter());
        setListViewListeners();
        if (data.getAdapter().isEmpty() && data.getFetchers().isEmpty()) {
            fillListViewWithData();
        }
    }

    @Override
    protected void fillListViewWithData() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getAdapter().clear();
                if(!data.isProgressDialogDisplayed()){
                    data.setProgressDialog(ProgressDialog.show(ProjectsActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.projects)));
                }
                SharedPreferences preferences = FavouritesUtil.getPreferences(ProjectsActivity.this);
                final Set<String> favProjects = new HashSet<String>(FavouritesUtil.getStringSet(preferences, ProjectsActivity.FAV_PROJECTS));
                if (FavouritesUtil.isFavouriteMode(getIntent())) {
                    toggleNavigationButtons(0, 0, 0);
                    ((ProjectsData)data).setCount(favProjects.size());
                    if (favProjects.isEmpty()) {
                        getAdapter().notifyDataSetChanged();
                        DialogsUtil.dismissDialog(data.getProgressDialog());
                        data.setProgressDialog(null);
                        toggleListViewVisibility(false);
                    } else {
                        toggleListViewVisibility(true);
                        for (String projectKey : favProjects) {
                            if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
                                data.getFetchers().add(new JSONProjectsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project/" + projectKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                            } else {
                                data.getQueue().add(new JSONProjectsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project/" + projectKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                            }
                        }
                    }
                } else {
                    toggleListViewVisibility(true);
                    getMoreData();
                }
            }
        });
    }

    @Override
    public void getMoreData() {
        if (! FavouritesUtil.isFavouriteMode(getIntent())) {
            clearDummyProjects();
        }
        if (data.getFetchers().size() < 10) {
            data.getFetchers().add(new JSONProjectsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project.json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), ""));
        } else {
            data.getQueue().add(new JSONProjectsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project.json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), ""));
        }
    }

    private void clearDummyProjects() {
        while(getAdapter().getPosition(Project.DUMMY_PROJECT)!=-1){
            getAdapter().remove(getAdapter().getItem(getAdapter().getPosition(Project.DUMMY_PROJECT)));
        }
        getAdapter().notifyDataSetChanged();
    }

    private void processProjects() {
        if (getAdapter().isEmpty()) {
            DialogsUtil.dismissDialog(data.getProgressDialog());
            data.setProgressDialog(null);
        }
        getAdapter().notifyLoadingDone();
        setListViewListeners();
    }

    private void setListViewListeners() {
        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent,
                                           View view,
                                           final int position,
                                           long id) {
                if (FavouritesUtil.isFavouriteMode(getIntent())) {
                    ((ProjectsData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(getAdapter().getItem(position).getKey(), false, ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this));
                    ((ProjectsData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                    if(((ProjectsData)data).getRemoveFromFavouritesDialog()!=null){
                        ((ProjectsData)data).getRemoveFromFavouritesDialog().show();
                    }
                } else {
                    if (FavouritesUtil.isFavourite(getAdapter().getItem(position).getKey(), ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this)) {
                        ((ProjectsData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(getAdapter().getItem(position).getKey(), false, ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this));
                        ((ProjectsData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                        if(((ProjectsData)data).getRemoveFromFavouritesDialog()!=null){
                            ((ProjectsData)data).getRemoveFromFavouritesDialog().show();
                        }
                    } else {
                        ((ProjectsData)data).setAddToFavouritesDialog(FavouritesUtil.createAddToFavouritesDialog(getAdapter().getItem(position).getKey(), false, ProjectsActivity.FAV_PROJECTS, ProjectsActivity.this));
                        ((ProjectsData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                        if(((ProjectsData)data).getAddToFavouritesDialog()!=null){
                            ((ProjectsData)data).getAddToFavouritesDialog().show();
                        }
                    }
                }
                return true;
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isOnline()) {
                    Intent intent = new Intent(ProjectsActivity.this, PlansActivity.class);
                    intent.putExtra(PROJECT_KEY, getAdapter().getItem(position).getKey());
                    intent.putExtra(LoginActivity.USER, getIntent().getStringExtra(LoginActivity.USER));
                    intent.putExtra(LoginActivity.PASS, getIntent().getStringExtra(LoginActivity.PASS));
                    intent.putExtra(SettingsActivity.FAVOURITE_MODE, false);
                    startActivity(intent);
                }
            }
        });
    }

    private class JSONProjectsFetcher extends JSONFetcher {

        public JSONProjectsFetcher(String... params) {
            super(params);
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (FavouritesUtil.isFavouriteMode(getIntent())) {
                        getAdapter().add(new Project(jsonObject));
                        getAdapter().notifyDataSetChanged();
                    } else {
                        JSONObject jsonObjectProjects = jsonObject.getJSONObject("projects");
                        int size = jsonObjectProjects.getInt("size");
                        int startIndex = jsonObjectProjects.getInt("start-index");
                        int maxSize = jsonObjectProjects.getInt("max-result");
                        boolean hasMoreData = toggleNavigationButtons(startIndex, maxSize, size);
                        if (! hasMoreData) {
                            getAdapter().notifyNoMoreData();
                        }
                        JSONArray jsonArray = jsonObjectProjects.getJSONArray("project");
                        List<Project> tmp = new ArrayList<Project>(jsonArray.length());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            tmp.add(new Project(jsonArray.getJSONObject(i)));
                        }
                        if (hasMoreData) {
                            tmp.add(Project.DUMMY_PROJECT);
                        }
                        getAdapter().addAll(tmp);
                        getAdapter().notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    //
                } finally {
                    ((ProjectsData)data).decreaseCount();
                    if (! FavouritesUtil.isFavouriteMode(getIntent()) || ((ProjectsData)data).getCount() == 0) {
                        processProjects();
                    }
                }
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                AlertDialog errorDialog = DialogsUtil.createErrorDialog(serverResponse.getCode(), ProjectsActivity.this);
                data.setErrorCode(serverResponse.getCode());
                data.setErrorDialog(errorDialog);
                errorDialog.show();
            }
        }
    }

    @Override
    protected void switchAdapter() {
        data.setStartIndex(SettingsActivity.DEFAULT_START_INDEX);
        getAdapter().setFavourite(FavouritesUtil.isFavouriteMode(getIntent()));
        getAdapter().notifyMoreData();
        getAdapter().notifyDataSetChanged();
    }

    private ProjectAdapter getAdapter() {
        return (ProjectAdapter) data.getAdapter();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        data.setProgressDialogDisplayed(data.getProgressDialog() != null && data.getProgressDialog().isShowing());
        data.setErrorDialogDisplayed(data.getErrorDialog() != null && data.getErrorDialog().isShowing());
        ((ProjectsData)data).setAddToFavouritesDialogDisplayed(((ProjectsData)data).getAddToFavouritesDialog() != null && ((ProjectsData)data).getAddToFavouritesDialog().isShowing());
        ((ProjectsData)data).setRemoveFromFavouritesDialogDisplayed(((ProjectsData)data).getRemoveFromFavouritesDialog() != null && ((ProjectsData)data).getRemoveFromFavouritesDialog().isShowing());
        DialogsUtil.dismissDialog(data.getProgressDialog());
        DialogsUtil.dismissDialog(data.getErrorDialog());
        DialogsUtil.dismissDialog(((ProjectsData)data).getAddToFavouritesDialog());
        DialogsUtil.dismissDialog(((ProjectsData)data).getRemoveFromFavouritesDialog());
        return data;
    }
}