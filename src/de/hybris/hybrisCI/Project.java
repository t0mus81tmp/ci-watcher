package de.hybris.hybrisCI;

import org.json.JSONException;
import org.json.JSONObject;

class Project {

    private static final String KEY = "key";
    private static final String NAME = "name";
    public static final Project DUMMY_PROJECT = new Project("dummy", "project");
    private final String name;
    private final String key;

    private Project(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public Project(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString(KEY), jsonObject.getString(NAME));
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Project)) {
            return false;
        }
        Project other = (Project) o;
        return getKey().equals(other.getKey()) && getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
