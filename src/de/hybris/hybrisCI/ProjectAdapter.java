package de.hybris.hybrisCI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;

class ProjectAdapter extends AbstractAdapter<Project> {

    public ProjectAdapter(Context context, List<Project> data, boolean favourite) {
        super(context, R.layout.plan_list_item, data, favourite);
        update();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ProjectHolder holder;

        if (! favourite && ! noMoreData && position + 1 == data.size()) {
            if (! loading) {
                getMoreData();
            }
            return ((Activity) getContext()).getLayoutInflater().inflate(R.layout.loading_list_item, parent, false);
        }

        if (row == null) {
            row = createRow(parent);
            holder = createProjectHolder(row);
            row.setTag(holder);
        } else {
            holder = (ProjectHolder) row.getTag();
            if (holder == null) {
                row = createRow(parent);
                holder = createProjectHolder(row);
                row.setTag(holder);
            }
        }

        Project project = data.get(position);
        holder.txtName.setText(project.getName());
        holder.txtKey.setText(project.getKey());
        holder.imgFavourite.setImageDrawable(getContext().getResources().getDrawable(favourites.contains(project.getKey()) ? R.drawable.icon_favourite_small : R.drawable.blank));
        return row;
    }

    @Override
    protected void update() {
        SharedPreferences preferences = context.getSharedPreferences(SettingsActivity.class.getName(), Context.MODE_PRIVATE);
        favourites = new HashSet<String>(FavouritesUtil.getStringSet(preferences, ProjectsActivity.FAV_PROJECTS));
    }

    private ProjectHolder createProjectHolder(View row) {
        ProjectHolder holder;
        holder = new ProjectHolder();
        holder.txtKey = (TextView) row.findViewById(R.id.key);
        holder.txtName = (TextView) row.findViewById(R.id.name);
        holder.imgFavourite = (ImageView) row.findViewById(R.id.favourite);
        return holder;
    }

    static class ProjectHolder {
        TextView txtKey;
        TextView txtName;
        ImageView imgFavourite;
    }

}
