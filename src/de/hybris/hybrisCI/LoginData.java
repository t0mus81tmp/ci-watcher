package de.hybris.hybrisCI;

import android.os.AsyncTask;

public class LoginData extends BaseData{

    private String user;
    private String pass;
    private AsyncTask<Void, Void, ServerResponse> fetcher;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public AsyncTask<Void, Void, ServerResponse> getFetcher() {
        return fetcher;
    }

    public void setFetcher(AsyncTask<Void, Void, ServerResponse> fetcher) {
        this.fetcher = fetcher;
    }
}
