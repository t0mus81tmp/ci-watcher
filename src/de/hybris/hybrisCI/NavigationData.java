package de.hybris.hybrisCI;

import android.app.AlertDialog;

public class NavigationData extends BaseData{

    private AlertDialog confirmationDialog;
    private boolean confirmationDialogDisplayed;

    public void setConfirmationDialog(AlertDialog confirmationDialog) {
        this.confirmationDialog = confirmationDialog;
    }

    public AlertDialog getConfirmationDialog() {
        return confirmationDialog;
    }

    public void setConfirmationDialogDisplayed(boolean confirmationDialogDisplayed) {
        this.confirmationDialogDisplayed = confirmationDialogDisplayed;
    }

    public boolean isConfirmationDialogDisplayed() {
        return confirmationDialogDisplayed;
    }
}
