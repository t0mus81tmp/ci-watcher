package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

class DialogsUtil {

    private static final String ERROR_MESSAGE_ID_PREFIX = "error_";
    private static final String DEF_TYPE_STRING = "string";

    public static AlertDialog createQuestionAlertDialog(final String message,
                                                        DialogInterface.OnClickListener listener,
                                                        Context context) {
        AlertDialog ad = createAlertDialog(message, listener, context);
        ad.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.cancel), listener);
        return ad;
    }

    public static AlertDialog createAlertDialog(final String message,
                                                DialogInterface.OnClickListener listener,
                                                Context context) {
        AlertDialog ad = new AlertDialog.Builder(context).create();
        ad.setCancelable(false);
        ad.setMessage(message);
        ad.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(R.string.ok), listener);
        return ad;
    }

    public static AlertDialog createErrorDialog(int code, final Context context) {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dismissDialog(dialog);
                        break;
                }
            }
        };
        String error_message = context.getString(R.string.error_);
        try {
            error_message = context.getString(context.getResources().getIdentifier(ERROR_MESSAGE_ID_PREFIX + String.valueOf(code), DEF_TYPE_STRING, context.getPackageName()));
        } catch (Exception e) {
            //
        }
        return DialogsUtil.createAlertDialog(context.getString(R.string.error) + " " + error_message, listener, context);
    }

    public static void dismissDialog(final DialogInterface dialog) {
        if(dialog!=null){
            try {
                dialog.dismiss();
            } catch (Exception e) {
                //
            }
        }
    }
}
