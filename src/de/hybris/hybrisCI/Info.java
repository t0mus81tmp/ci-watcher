package de.hybris.hybrisCI;

import org.json.JSONException;
import org.json.JSONObject;

public class Info {

    private static final String BUILD_TEST_SUMMARY = "buildTestSummary";
    private static final String SUCCESSFUL_TEST_COUNT = "successfulTestCount";
    private static final String FAILED_TEST_COUNT = "failedTestCount";
    private static final String QUARANTINED_TEST_COUNT = "quarantinedTestCount";

    private final String buildTestSummary;
    private final int successfulTestCount;
    private final int failedTestCount;
    private final int quarantinedTestCount;

    private Info(String buildTestSummary,
                 int successfulTestCount,
                 int failedTestCount,
                 int quarantinedTestCount) {
        this.buildTestSummary = buildTestSummary;
        this.successfulTestCount = successfulTestCount;
        this.failedTestCount = failedTestCount;
        this.quarantinedTestCount = quarantinedTestCount;
    }

    public Info(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString(BUILD_TEST_SUMMARY), jsonObject.getInt(SUCCESSFUL_TEST_COUNT), jsonObject.getInt(FAILED_TEST_COUNT), jsonObject.getInt(QUARANTINED_TEST_COUNT));
    }

    public String getBuildTestSummary() {
        return buildTestSummary;
    }

    public int getSuccessfulTestCount() {
        return successfulTestCount;
    }

    public int getFailedTestCount() {
        return failedTestCount;
    }

    public int getQuarantinedTestCount() {
        return quarantinedTestCount;
    }
}
