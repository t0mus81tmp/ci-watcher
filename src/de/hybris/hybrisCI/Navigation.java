package de.hybris.hybrisCI;

class Navigation {

    private final String text;
    private final String subtext;
    private final int iconID;

    public Navigation(String text, String subtext, int iconID) {
        this.text = text;
        this.iconID = iconID;
        this.subtext = subtext;
    }

    public String getText() {
        return text;
    }

    public String getSubtext() {
        return subtext;
    }

    public int getIconID() {
        return iconID;
    }
}
