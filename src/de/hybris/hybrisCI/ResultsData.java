package de.hybris.hybrisCI;

public class ResultsData extends BaseData {

    private int resultsCount;

    public void setResultsCount(int resultsCount) {
        this.resultsCount = resultsCount;
    }

    public int getResultsCount() {
        return resultsCount;
    }

    public void decreaseResultsCount() {
        --resultsCount;
    }
}
