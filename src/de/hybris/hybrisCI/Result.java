package de.hybris.hybrisCI;

import org.json.JSONException;
import org.json.JSONObject;

public class Result {

    private static final String KEY = "key";
    private static final String STATE = "state";
    private static final String LIFE_CYCLE_STATE = "lifeCycleState";
    private static final String NUMBER = "number";
    private static final String ID = "id";

    public static final String SUCCESSFUL = "Successful";
    public static final Result DUMMY_RESULT = new Result("dummy", "result", "dummy", - 1, - 1);

    private final String key;
    private final String state;
    private final String lifeCycleState;
    private final int number;
    private final int id;

    private Info latestInfo;

    private Result(String key, String state, String lifeCycleState, int number, int id) {
        this.key = key;
        this.state = state;
        this.lifeCycleState = lifeCycleState;
        this.number = number;
        this.id = id;
    }

    public Result(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString(KEY), jsonObject.getString(STATE), jsonObject.getString(LIFE_CYCLE_STATE), jsonObject.getInt(NUMBER), jsonObject.getInt(ID));
    }

    public String getKey() {
        return key;
    }

    public String getState() {
        return state;
    }

    public String getLifeCycleState() {
        return lifeCycleState;
    }

    public int getNumber() {
        return number;
    }

    public int getId() {
        return id;
    }

    public void setLatestInfo(Info latestInfo) {
        this.latestInfo = latestInfo;
    }

    public Info getLatestInfo() {
        return latestInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Result)) {
            return false;
        }
        Result other = (Result) o;
        return getKey().equals(other.getKey());
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
