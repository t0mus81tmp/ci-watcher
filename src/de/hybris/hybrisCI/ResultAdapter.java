package de.hybris.hybrisCI;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class ResultAdapter extends AbstractAdapter<Result> {

    public ResultAdapter(Context context, List<Result> data) {
        super(context, R.layout.result_list_item, data, false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ResultHolder holder;

        if (! noMoreData && position + 1 == data.size()) {
            if (! loading) {
                getMoreData();
            }
            return ((Activity) getContext()).getLayoutInflater().inflate(R.layout.loading_list_item, parent, false);
        }

        if (row == null) {
            row = createRow(parent);
            holder = createResultHolder(row);
            row.setTag(holder);
        } else {
            holder = (ResultHolder) row.getTag();
            if (holder == null) {
                row = createRow(parent);
                holder = createResultHolder(row);
                row.setTag(holder);
            }
        }

        Result result = data.get(position);
        if (result.getState().equals(Result.SUCCESSFUL)) {
            holder.imgStatus.setImageDrawable(getContext().getResources().getDrawable(R.drawable.icon_success_small));
        } else {
            holder.imgStatus.setImageDrawable(getContext().getResources().getDrawable(R.drawable.icon_failed_small));
        }
        holder.txtNumber.setText(String.valueOf(result.getNumber()));
        if (result.getLatestInfo() != null) {
            Spannable successful = new SpannableString(getContext().getString(R.string.successful) + "\n" + result.getLatestInfo().getSuccessfulTestCount());
            successful.setSpan(new StyleSpan(Typeface.BOLD), 0, getContext().getString(R.string.successful).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            successful.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.list_item_text_light)), getContext().getString(R.string.successful).length(), successful.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.txtSuccess.setText(successful);
            Spannable failed = new SpannableString(getContext().getString(R.string.failed) + "\n" + result.getLatestInfo().getFailedTestCount());
            failed.setSpan(new StyleSpan(Typeface.BOLD), 0, getContext().getString(R.string.failed).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            failed.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.list_item_text_light)), getContext().getString(R.string.failed).length(), failed.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.txtFailed.setText(failed);
            Spannable quarantined = new SpannableString(getContext().getString(R.string.quarantined) + "\n" + result.getLatestInfo().getQuarantinedTestCount());
            quarantined.setSpan(new StyleSpan(Typeface.BOLD), 0, getContext().getString(R.string.quarantined).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            quarantined.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.list_item_text_light)), getContext().getString(R.string.quarantined).length(), quarantined.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.txtQuarantined.setText(quarantined);
        }

        return row;
    }

    @Override
    protected void update() {
        //
    }

    private ResultHolder createResultHolder(View row) {
        ResultHolder holder;
        holder = new ResultHolder();
        holder.imgStatus = (ImageView) row.findViewById(R.id.status);
        holder.txtNumber = (TextView) row.findViewById(R.id.number);
        holder.txtSuccess = (TextView) row.findViewById(R.id.success);
        holder.txtFailed = (TextView) row.findViewById(R.id.failed);
        holder.txtQuarantined = (TextView) row.findViewById(R.id.quarantined);
        return holder;
    }

    static class ResultHolder {
        TextView txtNumber;
        ImageView imgStatus;
        TextView txtSuccess;
        TextView txtFailed;
        TextView txtQuarantined;
    }

}
