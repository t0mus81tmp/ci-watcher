package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SettingsActivity extends BaseActivity {

    public static final String HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST = "https://bamboo.hybris.com/rest/api/latest";
    public static final String FAVOURITE_MODE = "favourite";
    public static final String AUTH_TYPE = "basic";
    public static final int DEFAULT_MAX_RESULTS = 10;
    public static final int DEFAULT_START_INDEX = 0;
    public static final int SINGLE_RESULT = 1;
    public static final int DEFAULT_FETCHERS_COUNT = 10;
    public static final long DEFAULT_FETCHERS_INTERVAL = 150;

    private SettingsData data;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        data = (SettingsData) getLastCustomNonConfigurationInstance();
        SharedPreferences preferences = getSharedPreferences(SettingsActivity.class.getName(), MODE_PRIVATE);
        ((TextView) findViewById(R.id.user)).setText(preferences.getString(LoginActivity.USER, ""));
        ((TextView) findViewById(R.id.pass)).setText(preferences.getString(LoginActivity.PASS, ""));
        TextView maxResults = (TextView) findViewById(R.id.max_results2);
        maxResults.setText(String.valueOf(preferences.getInt(JSONFetcher.MAX_RESULTS, DEFAULT_MAX_RESULTS)));
        if (data == null) {
            data = new SettingsData();
            data.setTitleString(getResources().getString(R.string.settings));
            data.setAlertDialog(null);
        } else {
            if (data.getViewId() != - 1) {
                data.setAlertDialog(createAlertDialog(data.getViewId()));
                if (data.isAlertDialogVisible()) {
                    data.getAlertDialog().show();
                }
            }
        }
        getSupportActionBar().setTitle(data.getTitleString());
    }

    public void edit(final View view) {
        data.setViewId(view.getId());
        data.setAlertDialog(createAlertDialog(data.getViewId()));
        data.getAlertDialog().show();
    }

    private AlertDialog createAlertDialog(final int viewId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View promptsView = inflater.inflate(R.layout.settings_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.editText);
        final TextView textView = (TextView) promptsView.findViewById(R.id.textView);

        switch (viewId) {
            case R.id.user:
            case R.id.textView:
                textView.setText(getResources().getString(R.string.edit) + " " + getResources().getString(R.string.user));
                userInput.setText(((TextView) findViewById(R.id.user)).getText());
                userInput.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.pass:
            case R.id.textView1:
                textView.setText(getResources().getString(R.string.edit) + " " + getResources().getString(R.string.password));
                userInput.setText(((TextView) findViewById(R.id.pass)).getText());
                userInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case R.id.max_results2:
            case R.id.textView2:
                textView.setText(getResources().getString(R.string.edit) + " " + getResources().getString(R.string.max_results));
                userInput.setText(((TextView) findViewById(R.id.max_results2)).getText());
                userInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        userInput.selectAll();

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.set,
                        new DialogInterface.OnClickListener() {
                            private final SharedPreferences.Editor editor = getSharedPreferences(SettingsActivity.class.getName(), MODE_PRIVATE).edit();

                            public void onClick(DialogInterface dialog, int id) {
                                switch (viewId) {
                                    case R.id.user:
                                    case R.id.textView:
                                        ((TextView) findViewById(R.id.user)).setText(userInput.getText());
                                        editor.putString(LoginActivity.USER, userInput.getText().toString());
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                                            editor.apply();
                                        } else {
                                            editor.commit();
                                        }
                                        break;
                                    case R.id.pass:
                                    case R.id.textView1:
                                        ((TextView) findViewById(R.id.pass)).setText(userInput.getText());
                                        editor.putString(LoginActivity.PASS, userInput.getText().toString());
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                                            editor.apply();
                                        } else {
                                            editor.commit();
                                        }
                                        break;
                                    case R.id.max_results2:
                                    case R.id.textView2:
                                        try {
                                            editor.putInt(JSONFetcher.MAX_RESULTS, Integer.parseInt(userInput.getText().toString()));
                                            ((TextView) findViewById(R.id.max_results2)).setText(userInput.getText());
                                        } catch (NumberFormatException e) {
                                            editor.putInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS);
                                            ((TextView) findViewById(R.id.max_results2)).setText(String.valueOf(SettingsActivity.DEFAULT_MAX_RESULTS));
                                        }
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                                            editor.apply();
                                        } else {
                                            editor.commit();
                                        }
                                }
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        return alertDialogBuilder.create();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        data.setAlertDialogVisible(data.getAlertDialog()!=null && data.getAlertDialog().isShowing());
        DialogsUtil.dismissDialog(data.getAlertDialog());
        return data;
    }
}