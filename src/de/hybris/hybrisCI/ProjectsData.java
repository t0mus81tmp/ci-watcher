package de.hybris.hybrisCI;

import android.app.AlertDialog;

public class ProjectsData extends BaseData{

    private int count;
    private int errorCode;
    private AlertDialog errorDialog;
    private boolean errorDialogDisplayed=false;
    private AlertDialog addToFavouritesDialog;
    private String favouriteKey;
    private boolean addToFavouritesDialogDisplayed =false;
    private boolean removeFromFavouritesDialogDisplayed;
    private AlertDialog removeFromFavouritesDialog;
    private boolean forcedFavourite;

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorDialog(AlertDialog errorDialog) {
        this.errorDialog = errorDialog;
    }

    public AlertDialog getErrorDialog() {
        return errorDialog;
    }

    public void setErrorDialogDisplayed(boolean errorDialogDisplayed) {
        this.errorDialogDisplayed = errorDialogDisplayed;
    }

    public boolean isErrorDialogDisplayed() {
        return errorDialogDisplayed;
    }

    public void setAddToFavouritesDialog(AlertDialog addToFavouritesDialog) {
        this.addToFavouritesDialog = addToFavouritesDialog;
    }

    public AlertDialog getAddToFavouritesDialog() {
        return addToFavouritesDialog;
    }

    public void setFavouriteKey(String favouriteKey) {
        this.favouriteKey = favouriteKey;
    }

    public String getFavouriteKey() {
        return favouriteKey;
    }

    public void setAddToFavouritesDialogDisplayed(boolean addToFavouritesDialogDisplayed) {
        this.addToFavouritesDialogDisplayed = addToFavouritesDialogDisplayed;
    }

    public boolean isAddToFavouritesDialogDisplayed() {
        return addToFavouritesDialogDisplayed;
    }

    public void setRemoveFromFavouritesDialogDisplayed(boolean removeFromFavouritesDialogDisplayed) {
        this.removeFromFavouritesDialogDisplayed = removeFromFavouritesDialogDisplayed;
    }

    public boolean isRemoveFromFavouritesDialogDisplayed() {
        return removeFromFavouritesDialogDisplayed;
    }

    public void setRemoveFromFavouritesDialog(AlertDialog removeFromFavouritesDialog) {
        this.removeFromFavouritesDialog = removeFromFavouritesDialog;
    }

    public AlertDialog getRemoveFromFavouritesDialog() {
        return removeFromFavouritesDialog;
    }

    public void setForcedFavourite(boolean forcedFavourite) {
        this.forcedFavourite = forcedFavourite;
    }

    public boolean isForcedFavourite() {
        return forcedFavourite;
    }

    public void decreaseCount() {
        --count;
    }
}
