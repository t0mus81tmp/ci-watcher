package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class PlansActivity extends AbstractNavigableActivity {

    public static final String PLAN_KEY = "planKey";
    public static final String FAV_PLANS = "fav-plans";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plans);
        data = (PlansData) getLastCustomNonConfigurationInstance();
        ListView lv = (ListView) findViewById(R.id.listView);
        if (data == null) {
            data = new PlansData();
            data.setAdapter(new PlanAdapter(PlansActivity.this, new ArrayList<Plan>(0), FavouritesUtil.isFavouriteMode(getIntent())));
            ((PlansData)data).setFavouritePlansCount(0);
            ((PlansData)data).setPlansCount(0);
            ((PlansData)data).setBranchesCount(0);
            data.setTimer(new Timer());
            data.setHandler(new Handler());
            data.setFetchers(new ArrayList<JSONFetcher>(SettingsActivity.DEFAULT_FETCHERS_COUNT));
            data.setQueue(new ArrayList<JSONFetcher>());
            data.setStartIndex(SettingsActivity.DEFAULT_START_INDEX);
            if (FavouritesUtil.isFavouriteMode(getIntent())) {
                data.setTitleString(getResources().getString(R.string.favourite) + " " + getResources().getString(R.string.plans));
            } else {
                String projectKey = getIntent().getStringExtra(ProjectsActivity.PROJECT_KEY);
                if (projectKey != null) {
                    data.setTitleString(projectKey + " " + getResources().getString(R.string.plans));
                }
            }
        } else {
            if(data.isErrorDialogDisplayed()){
                data.setErrorDialog(DialogsUtil.createErrorDialog(data.getErrorCode(), PlansActivity.this));
                data.getErrorDialog().show();
            }else if(((PlansData)data).isAddToFavouritesDialogDisplayed()){
                ((PlansData)data).setAddToFavouritesDialog(FavouritesUtil.createAddToFavouritesDialog(((PlansData)data).getFavouriteKey(), ((PlansData)data).isForcedFavourite(), PlansActivity.FAV_PLANS, PlansActivity.this));
                ((PlansData)data).getAddToFavouritesDialog().show();
            }else if(((PlansData)data).isRemoveFromFavouritesDialogDisplayed()){
                ((PlansData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(((PlansData)data).getFavouriteKey(), ((PlansData)data).isForcedFavourite(), PlansActivity.FAV_PLANS, PlansActivity.this));
                ((PlansData)data).getRemoveFromFavouritesDialog().show();
            }else if(((PlansData)data).isAlertDialogDisplayed()){
                ((PlansData) data).setAlertDialog(createAlertDialog(((PlansData) data).getPosition()));
                ((PlansData) data).getAlertDialog().show();
            }else if(((PlansData) data).isSelectBranchDialogDisplayed()){
                ((PlansData) data).setSelectBranchDialog(createSelectBranchDialog(((PlansData) data).getPosition(),createIntent()));
                ((PlansData) data).getSelectBranchDialog().show();
            }else{
                if(data.isProgressDialogDisplayed()){
                    data.setProgressDialog(ProgressDialog.show(PlansActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.plans)));
                }
            }
        }
        getSupportActionBar().setTitle(data.getTitleString());
        lv.setAdapter(data.getAdapter());
        setListViewListeners();
        if (data.getAdapter().isEmpty() && data.getFetchers().isEmpty()) {
            fillListViewWithData();
        }
    }

    @Override
    protected void fillListViewWithData() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getAdapter().clear();
                SharedPreferences preferences = FavouritesUtil.getPreferences(PlansActivity.this);
                final Set<String> favouritePlans = new HashSet<String>(FavouritesUtil.getStringSet(preferences, PlansActivity.FAV_PLANS));
                if (FavouritesUtil.isFavouriteMode(getIntent())) {
                    toggleNavigationButtons(0, 0, 0);
                    if(!data.isProgressDialogDisplayed()){
                        data.setProgressDialog(ProgressDialog.show(PlansActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.plans)));
                    }
                    ((PlansData)data).setFavouritePlansCount(favouritePlans.size());
                    if (favouritePlans.isEmpty()) {
                        getAdapter().clear();
                        getAdapter().notifyDataSetChanged();
                        DialogsUtil.dismissDialog(data.getProgressDialog());
                        data.setProgressDialog(null);
                        toggleListViewVisibility(false);
                    } else {
                        toggleListViewVisibility(true);
                        for (String planKey : favouritePlans) {
                            if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
                                data.getFetchers().add(new JSONPlansFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/plan/" + planKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), "", "branches"));
                            } else {
                                data.getQueue().add(new JSONPlansFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/plan/" + planKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), "", "branches"));
                            }
                        }
                    }
                } else {
                    toggleListViewVisibility(true);
                    String projectKey = getIntent().getStringExtra(ProjectsActivity.PROJECT_KEY);
                    if (projectKey != null) {
                        data.setProgressDialog(ProgressDialog.show(PlansActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.plans)));
                        getMoreData();
                    } else {
                        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        DialogsUtil.dismissDialog(data.getProgressDialog());
                                        data.setProgressDialog(null);
                                        Intent intent = new Intent(PlansActivity.this, ProjectsActivity.class);
                                        intent.putExtra(LoginActivity.USER, getIntent().getStringExtra(LoginActivity.USER));
                                        intent.putExtra(LoginActivity.PASS, getIntent().getStringExtra(LoginActivity.PASS));
                                        intent.putExtra(SettingsActivity.FAVOURITE_MODE, false);
                                        startActivity(intent);
                                        finish();
                                        break;
                                }
                            }
                        };
                        ((PlansData)data).setQuestionDialog(DialogsUtil.createAlertDialog(getResources().getString(R.string.please_choose_project), listener, PlansActivity.this));
                        if(((PlansData) data).getQuestionDialog()!=null){
                            ((PlansData) data).getQuestionDialog().show();
                        }
                    }

                }

            }
        });
    }

    @Override
    public void getMoreData() {
        if (! FavouritesUtil.isFavouriteMode(getIntent()) && ! getAdapter().isEmpty()) {
            getAdapter().remove(getAdapter().getItem(getAdapter().getCount() - 1));
            getAdapter().notifyDataSetChanged();
        }
        String projectKey = getIntent().getStringExtra(ProjectsActivity.PROJECT_KEY);
        if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
            data.getFetchers().add(new JSONPlansFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project/" + projectKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), "plans"));
        } else {
            data.getQueue().add(new JSONPlansFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/project/" + projectKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), "plans"));
        }

    }

    private void processPlans() {
        if (getAdapter().isEmpty()) {
            toggleListViewVisibility(false);
            DialogsUtil.dismissDialog(data.getProgressDialog());
            data.setProgressDialog(null);
        }
        ((PlansData)data).setPlansCount(getAdapter().getCount());
        if (! FavouritesUtil.isFavouriteMode(getIntent())) {
            ((PlansData)data).decreasePlansCount();
        }
        for (int i = 0; i < getAdapter().getCount(); i++) {
            Plan plan = getAdapter().getItem(i);
            if (! plan.equals(Plan.DUMMY_PLAN)) {
                if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
                    data.getFetchers().add(new JSONResultsFetcher(plan, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + plan.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                } else {
                    data.getQueue().add(new JSONResultsFetcher(plan, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + plan.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                }

            }
        }
        ((PlansData)data).setBranchesCount(getAdapter().getCount());
        if (! FavouritesUtil.isFavouriteMode(getIntent())) {
            ((PlansData)data).decreaseBranchesCount();
            for (int i = 0; i < getAdapter().getCount(); i++) {
                Plan plan = getAdapter().getItem(i);
                if (! plan.equals(Plan.DUMMY_PLAN)) {
                    if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
                        data.getFetchers().add(new JSONBranchesFetcher(plan, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/plan/" + plan.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), "", "branches"));
                    } else {
                        data.getQueue().add(new JSONBranchesFetcher(plan, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/plan/" + plan.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), "", "branches"));
                    }
                }
            }
        }

    }

    private void updateListView() {
        ((PlansData)data).decreaseBranchesCount();
        ((PlansData)data).decreasePlansCount();
        if (((PlansData)data).getBranchesCount() <= 0 || ((PlansData)data).getPlansCount() <= 0) {
            getAdapter().notifyLoadingDone();
            setListViewListeners();
            DialogsUtil.dismissDialog(data.getProgressDialog());
            data.setProgressDialog(null);
        }
    }

    private void setListViewListeners() {
        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent,
                                           View view,
                                           final int position,
                                           long id) {
                if (FavouritesUtil.isFavouriteMode(getIntent())) {
                    ((PlansData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(getAdapter().getItem(position).getKey(), false, PlansActivity.FAV_PLANS, PlansActivity.this));
                    ((PlansData)data).setForcedFavourite(false);
                    ((PlansData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                    if(((PlansData)data).getRemoveFromFavouritesDialog()!=null){
                        ((PlansData)data).getRemoveFromFavouritesDialog().show();
                    }
                } else {
                    if (getAdapter().getItem(position).getBranchList().isEmpty() && FavouritesUtil.isFavourite(getAdapter().getItem(position).getKey(), PlansActivity.FAV_PLANS, PlansActivity.this)) {
                        ((PlansData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(getAdapter().getItem(position).getKey(), false, PlansActivity.FAV_PLANS, PlansActivity.this));
                        ((PlansData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                        ((PlansData)data).setForcedFavourite(false);
                        if(((PlansData)data).getRemoveFromFavouritesDialog()!=null){
                            ((PlansData)data).getRemoveFromFavouritesDialog().show();
                        }
                    } else {

                        if (getAdapter().getItem(position).getBranchList().isEmpty()) {
                            ((PlansData)data).setAddToFavouritesDialog(FavouritesUtil.createAddToFavouritesDialog(getAdapter().getItem(position).getKey(), false, PlansActivity.FAV_PLANS, PlansActivity.this));
                            ((PlansData)data).setForcedFavourite(false);
                            ((PlansData)data).setFavouriteKey(getAdapter().getItem(position).getKey());
                            if(((PlansData)data).getAddToFavouritesDialog()!=null){
                                ((PlansData)data).getAddToFavouritesDialog().show();
                            }
                        } else {
                            ((PlansData)data).setAlertDialog(createAlertDialog(position));
                            ((PlansData)data).setPosition(position);
                            if(((PlansData)data).getAlertDialog()!=null){
                                ((PlansData)data).getAlertDialog().show();
                            }
                        }

                    }

                }
                return true;
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (isOnline()) {
                    final Intent intent = createIntent();
                    if (FavouritesUtil.isFavouriteMode(getIntent()) || getAdapter().getItem(position).getBranchList().isEmpty()) {
                        intent.putExtra(PLAN_KEY, getAdapter().getItem(position).getKey());
                        intent.putExtra(LoginActivity.USER, getIntent().getStringExtra(LoginActivity.USER));
                        intent.putExtra(LoginActivity.PASS, getIntent().getStringExtra(LoginActivity.PASS));
                        startActivity(intent);
                    } else {
                        ((PlansData)data).setSelectBranchDialog(createSelectBranchDialog(position, intent));
                        ((PlansData) data).setPosition(position);
                        if(((PlansData)data).getSelectBranchDialog()!=null){
                            ((PlansData)data).getSelectBranchDialog().show();
                        }

                    }
                }
            }
        });
    }

    private Intent createIntent() {
        return new Intent(PlansActivity.this, ResultsActivity.class);
    }

    private AlertDialog createSelectBranchDialog(int position, final Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.select_branch));
        final String[] list = new String[getAdapter().getItem(position).getBranchList().size() + 1];
        final String[] keys = new String[list.length];
        keys[0] = getAdapter().getItem(position).getKey();
        list[0] = getAdapter().getItem(position).getShortName();
        for (int i = 1; i < list.length; i++) {
            keys[i] = getAdapter().getItem(position).getBranchList().get(i - 1).getKey();
            list[i] = getAdapter().getItem(position).getBranchList().get(i - 1).getShortName();
        }
        builder.setSingleChoiceItems(list, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                intent.putExtra(PLAN_KEY, keys[which]);
                intent.putExtra(LoginActivity.USER, getIntent().getStringExtra(LoginActivity.USER));
                intent.putExtra(LoginActivity.PASS, getIntent().getStringExtra(LoginActivity.PASS));
                startActivity(intent);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogsUtil.dismissDialog(dialog);
            }
        });
        return builder.create();
    }

    private AlertDialog createAlertDialog(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_to_favourites));
        final String[] list = new String[getAdapter().getItem(position).getBranchList().size() + 1];
        boolean[] checked = new boolean[list.length];
        final String[] keys = new String[list.length];
        keys[0] = getAdapter().getItem(position).getKey();
        list[0] = getAdapter().getItem(position).getShortName();
        checked[0] = FavouritesUtil.isFavourite(getAdapter().getItem(position).getKey(), PlansActivity.FAV_PLANS, this);
        for (int i = 1; i < list.length; i++) {
            keys[i] = getAdapter().getItem(position).getBranchList().get(i - 1).getKey();
            list[i] = getAdapter().getItem(position).getBranchList().get(i - 1).getShortName();
            checked[i] = FavouritesUtil.isFavourite(getAdapter().getItem(position).getBranchList().get(i - 1).getKey(), PlansActivity.FAV_PLANS, this);
        }
        builder.setMultiChoiceItems(list, checked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog,
                                int which,
                                boolean isChecked) {
                //
            }
        });
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SparseBooleanArray sba = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
                ListAdapter adapter = ((AlertDialog) dialog).getListView().getAdapter();
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (sba.get(i) && ! FavouritesUtil.isFavourite(keys[i], PlansActivity.FAV_PLANS, PlansActivity.this)) {
                        ((PlansData)data).setAddToFavouritesDialog(FavouritesUtil.createAddToFavouritesDialog(keys[i], true, PlansActivity.FAV_PLANS, PlansActivity.this));
                        ((PlansData)data).setFavouriteKey(keys[i]);
                        ((PlansData)data).setForcedFavourite(true);
                        if(((PlansData)data).getAddToFavouritesDialog()!=null){
                            ((PlansData)data).getAddToFavouritesDialog().show();
                        }
                    }
                    if (! sba.get(i) && FavouritesUtil.isFavourite(keys[i], PlansActivity.FAV_PLANS, PlansActivity.this)) {
                        ((PlansData)data).setRemoveFromFavouritesDialog(FavouritesUtil.createRemoveFromFavouritesDialog(keys[i], true, PlansActivity.FAV_PLANS, PlansActivity.this));
                        ((PlansData)data).setForcedFavourite(true);
                        if(((PlansData)data).getRemoveFromFavouritesDialog()!=null){
                            ((PlansData)data).getRemoveFromFavouritesDialog().show();
                        }
                    }

                }
                getAdapter().notifyDataSetChanged();

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogsUtil.dismissDialog(dialog);
            }
        });
        return builder.create();
    }

    private class JSONPlansFetcher extends JSONFetcher {
        public JSONPlansFetcher(String... params) {
            super(params);
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (FavouritesUtil.isFavouriteMode(getIntent())) {
                        getAdapter().add(new Plan(jsonObject));
                        getAdapter().notifyDataSetChanged();
                    } else {
                        JSONObject jsonObjectPlans = jsonObject.getJSONObject("plans");
                        int size = jsonObjectPlans.getInt("size");
                        int startIndex = jsonObjectPlans.getInt("start-index");
                        int maxSize = jsonObjectPlans.getInt("max-result");
                        boolean hasMoreData = toggleNavigationButtons(startIndex, maxSize, size);
                        if (! hasMoreData) {
                            getAdapter().notifyNoMoreData();
                        }
                        JSONArray jsonArray = jsonObjectPlans.getJSONArray("plan");
                        List<Plan> tmp = new ArrayList<Plan>(jsonArray.length());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            tmp.add(new Plan(jsonArray.getJSONObject(i)));
                        }
                        if (hasMoreData) {
                            tmp.add(Plan.DUMMY_PLAN);
                        }
                        getAdapter().addAll(tmp);
                    }
                } catch (JSONException e) {
                    //
                } finally {
                    ((PlansData)data).decreaseFavouritePlansCount();
                    if (! FavouritesUtil.isFavouriteMode(getIntent()) || ((PlansData)data).getFavouritePlansCount() == 0) {
                        processPlans();
                    }
                }
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                if(serverResponse.getCode()==StatusCodes.NOT_FOUND){
                    String planKey = this.params[0].substring(this.params[0].lastIndexOf("/")+1,this.params[0].lastIndexOf(".json"));
                    FavouritesUtil.removeFromFavourites(planKey,FAV_PLANS,PlansActivity.this);
                }else{
                    data.setErrorDialog(DialogsUtil.createErrorDialog(serverResponse.getCode(), PlansActivity.this));
                    data.setErrorCode(serverResponse.getCode());
                    data.getErrorDialog().show();
                }
            }
        }
    }

    private class JSONBranchesFetcher extends JSONFetcher {
        private final Plan plan;

        public JSONBranchesFetcher(Plan plan, String... params) {
            super(params);
            this.plan = plan;
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    plan.setBranchList(new Plan(jsonObject).getBranchList());
                } catch (JSONException e) {
                    //
                } finally {
                    getAdapter().notifyDataSetChanged();
                    updateListView();
                }
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                data.setErrorDialog(DialogsUtil.createErrorDialog(serverResponse.getCode(), PlansActivity.this));
                data.setErrorCode(serverResponse.getCode());
                data.getErrorDialog().show();
            }
        }
    }

    private class JSONResultsFetcher extends JSONFetcher {

        private final Plan plan;

        public JSONResultsFetcher(Plan plan, String... params) {
            super(params);
            this.plan = plan;
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONObject("results").getJSONArray("result");
                    plan.setLatestResult(new Result(jsonArray.getJSONObject(0)));
                } catch (JSONException e) {
                    //
                }
                if (FavouritesUtil.isFavouriteMode(getIntent())) {
                    updateListView();
                }
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                data.setErrorDialog(DialogsUtil.createErrorDialog(serverResponse.getCode(), PlansActivity.this));
                data.setErrorCode(serverResponse.getCode());
                data.getErrorDialog().show();
            }
        }
    }

    @Override
    protected void switchAdapter() {
        data.setStartIndex(SettingsActivity.DEFAULT_START_INDEX);
        getAdapter().setFavourite(FavouritesUtil.isFavouriteMode(getIntent()));
        getAdapter().notifyMoreData();
        getAdapter().notifyDataSetChanged();
    }

    private PlanAdapter getAdapter() {
        return (PlanAdapter) data.getAdapter();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        data.setProgressDialogDisplayed(data.getProgressDialog() != null && data.getProgressDialog().isShowing());
        data.setErrorDialogDisplayed(data.getErrorDialog() != null && data.getErrorDialog().isShowing());
        ((PlansData)data).setAddToFavouritesDialogDisplayed(((PlansData)data).getAddToFavouritesDialog() != null && ((PlansData)data).getAddToFavouritesDialog().isShowing());
        ((PlansData)data).setRemoveFromFavouritesDialogDisplayed(((PlansData)data).getRemoveFromFavouritesDialog() != null && ((PlansData)data).getRemoveFromFavouritesDialog().isShowing());
        ((PlansData)data).setAlertDialogDisplayed(((PlansData) data).getAlertDialog() != null && ((PlansData) data).getAlertDialog().isShowing());
        ((PlansData)data).setQuestionDialogDisplayed(((PlansData) data).getQuestionDialog() != null && ((PlansData) data).getQuestionDialog().isShowing());
        ((PlansData)data).setSelectBranchDialogDisplayed(((PlansData) data).getSelectBranchDialog() != null && ((PlansData) data).getSelectBranchDialog().isShowing());
        DialogsUtil.dismissDialog(((PlansData)data).getAlertDialog());
        DialogsUtil.dismissDialog(((PlansData)data).getSelectBranchDialog());
        DialogsUtil.dismissDialog(data.getErrorDialog());
        DialogsUtil.dismissDialog(data.getProgressDialog());
        DialogsUtil.dismissDialog(((PlansData)data).getQuestionDialog());
        DialogsUtil.dismissDialog(((PlansData)data).getAddToFavouritesDialog());
        DialogsUtil.dismissDialog(((PlansData)data).getRemoveFromFavouritesDialog());
        return data;
    }
}