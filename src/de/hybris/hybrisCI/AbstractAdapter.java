package de.hybris.hybrisCI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract class AbstractAdapter<T> extends ArrayAdapter<T> {
    protected final Context context;
    protected final int layoutResourceId;
    protected List<T> data;
    protected Set<String> favourites;
    protected boolean loading = true;
    protected boolean noMoreData = false;
    protected boolean favourite = false;

    public AbstractAdapter(Context context, int layoutResourceId, List<T> data, boolean favourite) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.favourite = favourite;
    }

    public View createRow(ViewGroup parent) {
        return ((Activity) context).getLayoutInflater().inflate(layoutResourceId, parent, false);
    }

    abstract protected void update();

    @Override
    public void notifyDataSetChanged() {
        update();
        super.notifyDataSetChanged();
    }

    public void notifyMoreData() {
        noMoreData = false;
    }

    public void notifyNoMoreData() {
        noMoreData = true;
    }

    public void notifyLoadingDone() {
        this.loading = false;
        notifyDataSetChanged();
    }

    @Override
    @SuppressLint("NewApi")
    public void addAll(Collection<? extends T> collection) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.addAll(collection);
        } else {
            for (T element : collection) {
                add(element);
            }
        }
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public void getMoreData() {
        loading = true;
        ((AbstractNavigableActivity) context).next(null);
        ((AbstractNavigableActivity) context).getMoreData();
    }

}
