package de.hybris.hybrisCI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class ResultsActivity extends AbstractNavigableActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plans);
        data = (ResultsData) getLastCustomNonConfigurationInstance();
        if (data == null) {
            data = new ResultsData();
            data.setAdapter(new ResultAdapter(ResultsActivity.this, new ArrayList<Result>(0)));
            ((ResultsData)data).setResultsCount(0);
            data.setTimer(new Timer());
            data.setHandler(new Handler());
            data.setFetchers(new ArrayList<JSONFetcher>(SettingsActivity.DEFAULT_FETCHERS_COUNT));
            data.setQueue(new ArrayList<JSONFetcher>());
            data.setStartIndex(SettingsActivity.DEFAULT_START_INDEX);
            String planKey = getIntent().getStringExtra(PlansActivity.PLAN_KEY);
            data.setTitleString(planKey + " " + getResources().getString(R.string.results));
        } else {
            if(data.isErrorDialogDisplayed()){
                data.setErrorDialog(DialogsUtil.createErrorDialog(data.getErrorCode(), ResultsActivity.this));
                data.getErrorDialog().show();
            }else{
                if(data.isProgressDialogDisplayed()){
                    data.setProgressDialog(ProgressDialog.show(ResultsActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.results)));
                }
            }
        }
        getSupportActionBar().setTitle(data.getTitleString());
        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(data.getAdapter());
        if (data.getAdapter().isEmpty()) {
            fillListViewWithData();
        }
    }

    @Override
    protected void fillListViewWithData() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getAdapter().clear();
                data.setProgressDialog(ProgressDialog.show(ResultsActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading) + " " + getResources().getString(R.string.results)));
                getMoreData();
            }
        });
    }

    @Override
    public void getMoreData() {
        if (! getAdapter().isEmpty()) {
            getAdapter().remove(getAdapter().getItem(getAdapter().getCount() - 1));
            getAdapter().notifyDataSetChanged();
        }
        String planKey = getIntent().getStringExtra(PlansActivity.PLAN_KEY);
        if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
            data.getFetchers().add(new JSONResultsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + planKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), ""));
        } else {
            data.getQueue().add(new JSONResultsFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + planKey + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(data.getStartIndex()), String.valueOf(FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS)), ""));
        }
    }

    private class JSONResultsFetcher extends JSONFetcher {

        public JSONResultsFetcher(String... params) {
            super(params);
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectResults = jsonObject.getJSONObject("results");
                    int size = jsonObjectResults.getInt("size");
                    int startIndex = jsonObjectResults.getInt("start-index");
                    int maxSize = jsonObjectResults.getInt("max-result");
                    boolean hasMoreData = toggleNavigationButtons(startIndex, maxSize, size);
                    if (! hasMoreData) {
                        getAdapter().notifyNoMoreData();
                    }
                    JSONArray jsonArray = jsonObjectResults.getJSONArray("result");
                    List<Result> tmp = new ArrayList<Result>(jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        tmp.add(new Result(jsonArray.getJSONObject(i)));
                    }
                    if (hasMoreData) {
                        tmp.add(Result.DUMMY_RESULT);
                    }
                    getAdapter().addAll(tmp);
                    getAdapter().notifyDataSetChanged();
                } catch (JSONException e) {

                } finally {
                    processResults();
                }
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                DialogsUtil.createErrorDialog(serverResponse.getCode(), ResultsActivity.this).show();
            }
        }
    }

    private void processResults() {
        if (getAdapter().isEmpty()) {
            toggleListViewVisibility(false);
            DialogsUtil.dismissDialog(data.getProgressDialog());
            data.setProgressDialog(null);
        }
        ((ResultsData)data).setResultsCount(getAdapter().getCount());
        for (int i = 0; i < getAdapter().getCount(); i++) {
            Result result = getAdapter().getItem(i);
            if (! result.equals(Result.DUMMY_RESULT)) {
                if (data.getFetchers().size() < SettingsActivity.DEFAULT_FETCHERS_COUNT) {
                    data.getFetchers().add(new JSONTestsFetcher(result, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + result.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                } else {
                    data.getQueue().add(new JSONTestsFetcher(result, SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/result/" + result.getKey() + ".json", SettingsActivity.AUTH_TYPE, getIntent().getStringExtra(LoginActivity.USER), getIntent().getStringExtra(LoginActivity.PASS), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), ""));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    private class JSONTestsFetcher extends JSONFetcher {

        private final Result result;

        public JSONTestsFetcher(Result result, String... params) {
            super(params);
            this.result = result;
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            String response = serverResponse.getResponse();
            data.getFetchers().remove(this);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    result.setLatestInfo(new Info(jsonObject));
                } catch (JSONException e) {

                }
                ((ResultsData)data).decreaseResultsCount();
                if (((ResultsData)data).getResultsCount() == 0) {
                    getAdapter().notifyLoadingDone();
                    DialogsUtil.dismissDialog(data.getProgressDialog());
                    data.setProgressDialog(null);
                }
            } else {
                DialogsUtil.dismissDialog(data.getProgressDialog());
                data.setProgressDialog(null);
                DialogsUtil.createErrorDialog(serverResponse.getCode(), ResultsActivity.this).show();
            }
        }
    }


    @Override
    protected void switchAdapter() {
        //
    }

    private ResultAdapter getAdapter() {
        ListView lv = (ListView) findViewById(R.id.listView);
        return (ResultAdapter) lv.getAdapter();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        data.setProgressDialogDisplayed(data.getProgressDialog()!=null&&data.getProgressDialog().isShowing());
        DialogsUtil.dismissDialog(data.getProgressDialog());
        return data;
    }

}