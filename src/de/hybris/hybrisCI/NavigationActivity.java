package de.hybris.hybrisCI;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class NavigationActivity extends BaseActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation);
        data = (NavigationData) getLastCustomNonConfigurationInstance();
        if(data==null){
            data = new NavigationData();
        }else{
            if(data.isErrorDialogDisplayed()){
                data.setErrorDialog(DialogsUtil.createErrorDialog(666, NavigationActivity.this));
                data.getErrorDialog().show();
            }else if(((NavigationData)data).isConfirmationDialogDisplayed()){
                createConfirmationDialog();
                ((NavigationData)data).getConfirmationDialog().show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.navigation);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new NavigationAdapter(this, createNavigationItems()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        start(ProjectsActivity.class, false);
                        break;
                    case 1:
                        start(ProjectsActivity.class, true);
                        break;
                    case 2:
                        start(PlansActivity.class, true);
                        break;
                }
            }
        });
    }

    private List<Navigation> createNavigationItems() {
        SharedPreferences preferences = FavouritesUtil.getPreferences(this);
        List<Navigation> result = new ArrayList<Navigation>(3);
        result.add(new Navigation(getResources().getString(R.string.browse_all_projects), null, R.drawable.icon_browse_all_projects));
        result.add(new Navigation(getResources().getString(R.string.browse_watched_projects), getResources().getString(R.string.you_are_watching) + " " + getFavouritesCount(preferences, ProjectsActivity.FAV_PROJECTS) + " " + getResources().getString(R.string.project_s), R.drawable.icon_browse_watched_projects));
        result.add(new Navigation(getResources().getString(R.string.browse_watched_plans), getResources().getString(R.string.you_are_watching) + " " + getFavouritesCount(preferences, PlansActivity.FAV_PLANS) + " " + getResources().getString(R.string.plan_s), R.drawable.icon_browse_watched_plans));
        return result;
    }

    private int getFavouritesCount(SharedPreferences preferences, String key) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return preferences.getStringSet(key, new HashSet<String>()).size();
        } else {
            int count = 0;
            String prefix = key + "_";
            for (Map.Entry<String, ?> entry : preferences.getAll().entrySet()) {
                if (entry.getKey().startsWith(prefix)) {
                    count++;
                }
            }
            return count;
        }
    }

    void start(Class targetActivity, boolean favouriteMode) {
        if (isOnline()) {
            Intent intent = new Intent(NavigationActivity.this, targetActivity);
            intent.putExtra(LoginActivity.USER, getIntent().getStringExtra(LoginActivity.USER));
            intent.putExtra(LoginActivity.PASS, getIntent().getStringExtra(LoginActivity.PASS));
            intent.putExtra(SettingsActivity.FAVOURITE_MODE, favouriteMode);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        createConfirmationDialog();
        ((NavigationData)data).getConfirmationDialog().show();
    }

    private void createConfirmationDialog() {
        ((NavigationData)data).setConfirmationDialog(DialogsUtil.createQuestionAlertDialog(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        NavigationActivity.this.finish();
                        break;
                }
            }
        }, NavigationActivity.this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
        return true;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        ((NavigationData)data).setConfirmationDialogDisplayed(((NavigationData)data).getConfirmationDialog() != null && ((NavigationData)data).getConfirmationDialog().isShowing());
        data.setErrorDialogDisplayed(data.getErrorDialog()!=null&&data.getErrorDialog().isShowing());
        DialogsUtil.dismissDialog(((NavigationData)data).getConfirmationDialog());
        DialogsUtil.dismissDialog(data.getErrorDialog());
        return data;
    }
}