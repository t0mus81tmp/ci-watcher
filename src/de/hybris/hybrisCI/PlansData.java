package de.hybris.hybrisCI;

import android.app.AlertDialog;

public class PlansData extends ProjectsData {

    private int favouritePlansCount;
    private int plansCount;
    private int branchesCount;
    private AlertDialog questionDialog;
    private boolean questionDialogDisplayed;
    private boolean alertDialogDisplayed;
    private AlertDialog alertDialog;
    private int position;
    private AlertDialog selectBranchDialog;
    private boolean selectBranchDialogDisplayed;

    public void setFavouritePlansCount(int favouritePlansCount) {
        this.favouritePlansCount = favouritePlansCount;
    }

    public int getFavouritePlansCount() {
        return favouritePlansCount;
    }

    public void setPlansCount(int plansCount) {
        this.plansCount = plansCount;
    }

    public int getPlansCount() {
        return plansCount;
    }

    public void setBranchesCount(int branchesCount) {
        this.branchesCount = branchesCount;
    }

    public int getBranchesCount() {
        return branchesCount;
    }

    public void setQuestionDialog(AlertDialog questionDialog) {
        this.questionDialog = questionDialog;
    }

    public AlertDialog getQuestionDialog() {
        return questionDialog;
    }

    public void setQuestionDialogDisplayed(boolean questionDialogDisplayed) {
        this.questionDialogDisplayed = questionDialogDisplayed;
    }

    public boolean isQuestionDialogDisplayed() {
        return questionDialogDisplayed;
    }

    public void setAlertDialogDisplayed(boolean alertDialogDisplayed) {
        this.alertDialogDisplayed = alertDialogDisplayed;
    }

    public boolean isAlertDialogDisplayed() {
        return alertDialogDisplayed;
    }

    public void setAlertDialog(AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    public AlertDialog getAlertDialog() {
        return alertDialog;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setSelectBranchDialog(AlertDialog selectBranchDialog) {
        this.selectBranchDialog = selectBranchDialog;
    }

    public AlertDialog getSelectBranchDialog() {
        return selectBranchDialog;
    }

    public void setSelectBranchDialogDisplayed(boolean selectBranchDialogDisplayed) {
        this.selectBranchDialogDisplayed = selectBranchDialogDisplayed;
    }

    public boolean isSelectBranchDialogDisplayed() {
        return selectBranchDialogDisplayed;
    }

    public void decreaseFavouritePlansCount() {
        --favouritePlansCount;
    }

    public void decreaseBranchesCount() {
        --branchesCount;
    }

    public void decreasePlansCount() {
        --plansCount;
    }
}
