package de.hybris.hybrisCI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class FavouritesUtil {

    public static void toggleFavouriteMode(Intent intent) {
        intent.putExtra(SettingsActivity.FAVOURITE_MODE, ! isFavouriteMode(intent));
    }

    public static boolean isFavouriteMode(Intent intent) {
        return intent.getBooleanExtra(SettingsActivity.FAVOURITE_MODE, false);
    }

    public static boolean isFavourite(String key, String name, Context context) {
        final SharedPreferences preferences = getPreferences(context);
        final Set<String> favouritePlans = new HashSet<String>(FavouritesUtil.getStringSet(preferences, name));
        return favouritePlans.contains(key);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(SettingsActivity.class.getName(), Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getPreferencesEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static AlertDialog createAddToFavouritesDialog(final String planKey,
                                                          final boolean force,
                                                          final String name,
                                                          final Context context) {
        final SharedPreferences preferences = FavouritesUtil.getPreferences(context);
        final Set<String> favourites = new HashSet<String>(FavouritesUtil.getStringSet(preferences, name));
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        favourites.add(planKey);
                        DialogsUtil.dismissDialog(dialog);
                        FavouritesUtil.saveFavourites(preferences, favourites, name);
                        ListView lv = (ListView) ((Activity) context).findViewById(R.id.listView);
                        ((ArrayAdapter) lv.getAdapter()).notifyDataSetChanged();
                        break;
                }
            }
        };
        if (force) {
            favourites.add(planKey);
            FavouritesUtil.saveFavourites(preferences, favourites, name);
        } else {
            return DialogsUtil.createQuestionAlertDialog(context.getResources().getString(R.string.add_to_favourites), listener, context);
        }
        return null;
    }

    public static void removeFromFavourites(final String planKey,
                                            final String name,
                                            final Context context) {
        final SharedPreferences preferences = FavouritesUtil.getPreferences(context);
        final Set<String> favourites = new HashSet<String>(FavouritesUtil.getStringSet(preferences, name));
        favourites.remove(planKey);
        FavouritesUtil.saveFavourites(preferences, favourites, name);
    }

    public static AlertDialog createRemoveFromFavouritesDialog(final String planKey,
                                                               final boolean force,
                                                               final String name,
                                                               final Context context) {
        final SharedPreferences preferences = FavouritesUtil.getPreferences(context);
        final Set<String> favourites = new HashSet<String>(FavouritesUtil.getStringSet(preferences, name));
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        favourites.remove(planKey);
                        DialogsUtil.dismissDialog(dialog);
                        FavouritesUtil.saveFavourites(preferences, favourites, name);
                        if (FavouritesUtil.isFavouriteMode(((Activity) context).getIntent())) {
                            ((AbstractNavigableActivity) context).fillListViewWithData();
                        } else {
                            ListView lv = (ListView) ((Activity) context).findViewById(R.id.listView);
                            ((ArrayAdapter) lv.getAdapter()).notifyDataSetChanged();
                        }
                        break;
                }
            }
        };
        if (force) {
            favourites.remove(planKey);
            FavouritesUtil.saveFavourites(preferences, favourites, name);
        } else {
            return DialogsUtil.createQuestionAlertDialog(context.getResources().getString(R.string.remove_from_favourites), listener, context);
        }
        return null;
    }

    private static void saveFavourites(SharedPreferences preferences,
                                       Set<String> favProjects,
                                       String name) {
        SharedPreferences.Editor editor = preferences.edit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            editor.putStringSet(name, favProjects);
        } else {
            String key = name + "_";
            int i = 0;
            for (Map.Entry<String, ?> entry : preferences.getAll().entrySet()) {
                if (entry.getKey().startsWith(key)) {
                    editor.remove(entry.getKey());
                }
            }
            editor.commit();
            for (String value : favProjects) {
                editor.putString(key + (i++), value);
            }
        }
        editor.commit();
    }

    public static Set<String> getStringSet(SharedPreferences preferences, String name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return preferences.getStringSet(name, new HashSet<String>());
        } else {
            String key = name + "_";
            HashSet<String> result = new HashSet<String>();
            for (Map.Entry<String, ?> entry : preferences.getAll().entrySet()) {
                if (entry.getKey().startsWith(key)) {
                    result.add((String) entry.getValue());
                }
            }
            return result;
        }
    }
}
