package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Timer;

public class BaseData {

    private int errorCode;
    private AlertDialog errorDialog;
    private boolean errorDialogDisplayed;
    private String titleString;
    private AbstractAdapter adapter;
    private Timer timer;
    private Handler handler;
    private ArrayList<JSONFetcher> fetchers;
    private ArrayList<JSONFetcher> queue;
    private int startIndex;
    private ProgressDialog progressDialog;
    private boolean progressDialogDisplayed=false;

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setAdapter(AbstractAdapter adapter) {
        this.adapter = adapter;
    }

    public AbstractAdapter getAdapter() {
        return adapter;
    }

    public String getTitleString() {
        return titleString;
    }

    public void setTitleString(String titleString) {
        this.titleString = titleString;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setFetchers(ArrayList<JSONFetcher> fetchers) {
        this.fetchers = fetchers;
    }

    public ArrayList<JSONFetcher> getFetchers() {
        return fetchers;
    }

    public void setQueue(ArrayList<JSONFetcher> queue) {
        this.queue = queue;
    }

    public ArrayList<JSONFetcher> getQueue() {
        return queue;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setProgressDialogDisplayed(boolean progressDialogDisplayed) {
        this.progressDialogDisplayed = progressDialogDisplayed;
    }

    public boolean isProgressDialogDisplayed() {
        return progressDialogDisplayed;
    }

    public AlertDialog getErrorDialog() {
        return errorDialog;
    }

    public void setErrorDialog(AlertDialog errorDialog) {
        this.errorDialog = errorDialog;
    }

    public void setErrorDialogDisplayed(boolean displayed) {
        this.errorDialogDisplayed = displayed;
    }

    public boolean isErrorDialogDisplayed() {
        return errorDialogDisplayed;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
