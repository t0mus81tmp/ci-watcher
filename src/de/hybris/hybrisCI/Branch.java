package de.hybris.hybrisCI;

import org.json.JSONException;
import org.json.JSONObject;

class Branch implements Comparable<Branch> {
    private final static String KEY = "key";
    private final static String NAME = "name";
    private final static String SHORT_KEY = "shortKey";
    private final static String SHORT_NAME = "shortName";
    private final static String DESCRIPTION = "description";
    private final static String ENABLED = "enabled";

    private final String description;
    private final String shortKey;
    private final String shortName;
    private final boolean enabled;
    private final String key;
    private final String name;

    private Branch(String description,
                   String shortKey,
                   String shortName,
                   boolean enabled,
                   String key,
                   String name) {
        this.description = description;
        this.shortKey = shortKey;
        this.shortName = shortName;
        this.enabled = enabled;
        this.key = key;
        this.name = name;
    }

    public Branch(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString(DESCRIPTION), jsonObject.getString(SHORT_KEY), jsonObject.getString(SHORT_NAME), jsonObject.getBoolean(ENABLED), jsonObject.getString(KEY), jsonObject.getString(NAME));
    }

    public String getDescription() {
        return description;
    }

    public String getShortKey() {
        return shortKey;
    }

    public String getShortName() {
        return shortName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Branch another) {
        return this.getShortName().compareTo(another.getShortName());
    }

    @Override
    public String toString() {
        return "[Branch] key:" + key + ", name: " + name + "]";
    }
}
