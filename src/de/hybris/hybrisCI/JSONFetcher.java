package de.hybris.hybrisCI;

import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

class JSONFetcher extends AsyncTask<Void, Void, ServerResponse> {

    public static final String START_INDEX = "start-index";
    public static final String MAX_RESULTS = "max-results";
    private static final String OS_AUTH_TYPE = "os_authType";
    private static final String OS_USERNAME = "os_username";
    private static final String OS_PASSWORD = "os_password";
    private static final String EXPAND = "expand";
    final String[] params;

    public JSONFetcher(String... params) {
        this.params = params;
    }

    HttpResponse executeHttpRequest(String url) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        return httpclient.execute(httpget);
    }

    String readResponse(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder result = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result.append(line).append("\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {

            }
        }
        return result.toString();
    }

    protected ServerResponse doInBackground(Void... voids) {
        try {
            HttpResponse httpResponse = executeHttpRequest(getUrl(params));
            if (httpResponse.getStatusLine() != null) {
                int code = httpResponse.getStatusLine().getStatusCode();
                switch (code) {
                    case StatusCodes.OK:
                        return new ServerResponse(readResponse(httpResponse.getEntity().getContent()), StatusCodes.OK);
                    default:
                        return new ServerResponse(null, code);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ServerResponse(null, - 1);
    }

    private String getUrl(String[] params) {
        StringBuilder result = new StringBuilder(50);
        result.append(params[0])
                .append("?" + OS_AUTH_TYPE + "=").append(params[1])
                .append("&" + OS_USERNAME + "=").append(params[2])
                .append("&" + OS_PASSWORD + "=").append(params[3])
                .append("&" + START_INDEX + "=").append(params[4])
                .append("&" + MAX_RESULTS + "=").append(params[5])
                .append("&" + EXPAND + "=").append(params[6]);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JSONFetcher that = (JSONFetcher) o;

        return Arrays.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(params);
    }
}
