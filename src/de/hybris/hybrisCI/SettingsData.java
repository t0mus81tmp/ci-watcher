package de.hybris.hybrisCI;

import android.app.AlertDialog;

public class SettingsData extends BaseData {

    private boolean alertDialogVisible;
    private int viewId;
    private AlertDialog alertDialog;

    public boolean isAlertDialogVisible() {
        return alertDialogVisible;
    }

    public void setAlertDialogVisible(boolean alertDialogVisible) {
        this.alertDialogVisible = alertDialogVisible;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }

    public void setAlertDialog(AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    public AlertDialog getAlertDialog() {
        return alertDialog;
    }
}
