package de.hybris.hybrisCI;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class NavigationAdapter extends AbstractAdapter<Navigation> {

    public NavigationAdapter(Context context, List<Navigation> data) {
        super(context, R.layout.navigation_list_item, data, false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        NavigationHolder holder;

        if (row == null) {
            row = createRow(parent);
            holder = createNavigationHolder(row);
            row.setTag(holder);
        } else {
            holder = (NavigationHolder) row.getTag();
        }

        Navigation navigation = data.get(position);
        holder.text.setText(navigation.getText());
        if (navigation.getSubtext() == null) {
            holder.subtext.setVisibility(View.GONE);
        } else {
            holder.subtext.setText(navigation.getSubtext());
        }
        holder.icon.setImageDrawable(getContext().getResources().getDrawable(navigation.getIconID()));
        return row;
    }

    @Override
    protected void update() {
        //
    }

    private NavigationHolder createNavigationHolder(View row) {
        NavigationHolder holder;
        holder = new NavigationHolder();
        holder.text = (TextView) row.findViewById(R.id.text);
        holder.subtext = (TextView) row.findViewById(R.id.subtext);
        holder.icon = (ImageView) row.findViewById(R.id.imageView);
        return holder;
    }

    static class NavigationHolder {
        private TextView text;
        private TextView subtext;
        private ImageView icon;
    }
}
