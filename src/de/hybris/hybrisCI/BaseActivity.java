package de.hybris.hybrisCI;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

abstract class BaseActivity extends ActionBarActivity {

    public static final int CONNECTION_ERROR_CODE = 666;
    protected BaseData data;

    boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        data.setErrorDialog(DialogsUtil.createErrorDialog(CONNECTION_ERROR_CODE, this));
        data.setErrorCode(CONNECTION_ERROR_CODE);
        if(data.getErrorDialog()!=null){
            data.getErrorDialog().show();
        }
        return false;
    }

    void toggleListViewVisibility(boolean show) {
        if (show) {
            findViewById(R.id.listView).setVisibility(View.VISIBLE);
            findViewById(R.id.textView).setVisibility(View.GONE);
        } else {
            findViewById(R.id.listView).setVisibility(View.GONE);
            findViewById(R.id.textView).setVisibility(View.VISIBLE);
        }
    }
}
