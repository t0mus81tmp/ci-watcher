package de.hybris.hybrisCI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;

class PlanAdapter extends AbstractAdapter<Plan> {

    public PlanAdapter(Context context, List<Plan> data, boolean favourite) {
        super(context, R.layout.plan_list_item, data, favourite);
        update();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PlanHolder holder;

        if (! favourite && ! noMoreData && position + 1 == data.size()) {
            if (! loading) {
                getMoreData();
            }
            return ((Activity) getContext()).getLayoutInflater().inflate(R.layout.loading_list_item, parent, false);
        }

        if (row == null) {
            row = createRow(parent);
            holder = createPlanHolder(row);
            row.setTag(holder);
        } else {
            holder = (PlanHolder) row.getTag();
            if (holder == null) {
                row = createRow(parent);
                holder = createPlanHolder(row);
                row.setTag(holder);
            }
        }

        Plan plan = data.get(position);
        holder.txtName.setText(plan.getShortName());
        holder.txtKey.setText(plan.getKey());
        String latestResult = plan.getLatestResult() != null ? plan.getLatestResult().getState() : context.getResources().getString(R.string.unknown);
        holder.imgStatus.setImageDrawable(context.getResources().getString(R.string.successful).equals(latestResult) ? context.getResources().getDrawable(R.drawable.icon_success_small) : context.getResources().getString(R.string.failed).equals(latestResult) ? context.getResources().getDrawable(R.drawable.icon_failed_small) : context.getResources().getDrawable(R.drawable.icon_neutral_small));
        holder.imgFavourite.setImageDrawable(getContext().getResources().getDrawable(isFavourite(plan) ? R.drawable.icon_favourite_small : R.drawable.blank));
        holder.imgBranch.setImageDrawable(getContext().getResources().getDrawable(plan.getBranchList().isEmpty() ? R.drawable.blank : R.drawable.icon_branch_small));
        return row;
    }

    @Override
    protected void update() {
        SharedPreferences preferences = context.getSharedPreferences(SettingsActivity.class.getName(), Context.MODE_PRIVATE);
        favourites = new HashSet<String>(FavouritesUtil.getStringSet(preferences, PlansActivity.FAV_PLANS));
    }

    private PlanHolder createPlanHolder(View row) {
        PlanHolder holder;
        holder = new PlanHolder();
        holder.txtKey = (TextView) row.findViewById(R.id.key);
        holder.txtName = (TextView) row.findViewById(R.id.name);
        holder.imgStatus = (ImageView) row.findViewById(R.id.status);
        holder.imgFavourite = (ImageView) row.findViewById(R.id.favourite);
        holder.imgBranch = (ImageView) row.findViewById(R.id.branch);
        return holder;
    }

    private boolean isFavourite(Plan plan) {
        if (favourites.contains(plan.getKey())) {
            return true;
        }
        for (Branch branch : plan.getBranchList()) {
            if (favourites.contains(branch.getKey())) {
                return true;
            }
        }
        return false;
    }

    static class PlanHolder {
        TextView txtKey;
        TextView txtName;
        ImageView imgBranch;
        ImageView imgStatus;
        ImageView imgFavourite;
    }

}
