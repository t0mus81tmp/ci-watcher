package de.hybris.hybrisCI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Plan {

    private static final String KEY = "key";
    private static final String NAME = "name";
    private static final String SHORT_KEY = "shortKey";
    private static final String SHORT_NAME = "shortName";
    private static final String ENABLED = "enabled";
    public static Plan DUMMY_PLAN = new Plan("dummy", "plan", "dummy", "plan", true);

    private final String key;
    private final String name;
    private final String shortKey;
    private final String shortName;
    private final boolean enabled;
    private Result latestResult;
    private List<Branch> branchList = new ArrayList<Branch>();

    private Plan(String key, String name, String shortKey, String shortName, boolean enabled) {
        this.key = key;
        this.name = name;
        this.shortKey = shortKey;
        this.shortName = shortName;
        this.enabled = enabled;
    }

    public Plan(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString(KEY), jsonObject.getString(NAME), jsonObject.getString(SHORT_KEY), jsonObject.getString(SHORT_NAME), jsonObject.getBoolean(ENABLED));
        try {
            JSONObject branches = jsonObject.getJSONObject("branches");
            JSONArray branchArray = branches.getJSONArray("branch");
            for (int i = 0; i < branchArray.length(); i++) {
                branchList.add(new Branch(branchArray.getJSONObject(i)));
            }
            Collections.sort(branchList);
        } catch (JSONException e) {

        }

    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getShortKey() {
        return shortKey;
    }

    public String getShortName() {
        return shortName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setLatestResult(Result latestResult) {
        this.latestResult = latestResult;
    }

    public Result getLatestResult() {
        return latestResult;
    }

    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Plan)) {
            return false;
        }
        Plan other = (Plan) o;
        return getKey().equals(other.getKey()) && getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
