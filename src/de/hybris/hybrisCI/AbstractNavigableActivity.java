package de.hybris.hybrisCI;

import android.content.Intent;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Timer;
import java.util.TimerTask;

abstract class AbstractNavigableActivity extends BaseActivity {

    protected TimerTask doAsynchronousTask;

    protected abstract void fillListViewWithData();

    protected abstract void getMoreData();

    public void next(View view) {
        data.setStartIndex(data.getStartIndex()+ FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS));
        //getIntent().putExtra(JSONFetcher.START_INDEX, startIndex);
        //fillListViewWithData();
    }

    public void previous(View view) {
        data.setStartIndex(data.getStartIndex()-FavouritesUtil.getPreferences(this).getInt(JSONFetcher.MAX_RESULTS, SettingsActivity.DEFAULT_MAX_RESULTS));
        //getIntent().putExtra(JSONFetcher.START_INDEX, startIndex >= SettingsActivity.DEFAULT_START_INDEX ? startIndex : SettingsActivity.DEFAULT_START_INDEX);
        //fillListViewWithData();
    }

    boolean toggleNavigationButtons(int startIndex, int maxSize, int size) {
        if (startIndex == 0 && maxSize == 0 && size == 0) {
            findViewById(R.id.navigation_area).setVisibility(View.GONE);
            return false;
        } else {
            findViewById(R.id.navigation_area).setVisibility(View.GONE);
        }

        Button next = (Button) findViewById(R.id.next);
        if (startIndex + maxSize >= size) {
            next.setBackgroundColor(getResources().getColor(R.color.navigation_button_disabled));
            next.setTextColor(getResources().getColor(R.color.navigation_button_text_disabled));
            next.setEnabled(false);
            return false;
        } else {
            next.setBackgroundColor(getResources().getColor(R.color.navigation_button_enabled));
            next.setTextColor(getResources().getColor(android.R.color.white));
            next.setEnabled(true);
        }
        Button previous = (Button) findViewById(R.id.previous);
        if (startIndex == SettingsActivity.DEFAULT_START_INDEX) {
            previous.setBackgroundColor(getResources().getColor(R.color.navigation_button_disabled));
            previous.setTextColor(getResources().getColor(R.color.navigation_button_text_disabled));
            previous.setEnabled(false);
        } else {
            previous.setBackgroundColor(getResources().getColor(R.color.navigation_button_enabled));
            previous.setTextColor(getResources().getColor(android.R.color.white));
            previous.setEnabled(true);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.favourite:
                FavouritesUtil.toggleFavouriteMode(getIntent());
                switchAdapter();
                fillListViewWithData();
                break;
        }
        return true;
    }

    protected abstract void switchAdapter();

    protected TimerTask createTask(final Handler handler) {
        return doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (! data.getFetchers().isEmpty()) {
                                data.getFetchers().get(0).execute();
                            }
                            if (! data.getQueue().isEmpty()) {
                                data.getFetchers().add(data.getQueue().get(0));
                                data.getQueue().remove(0);
                            }
                        } catch (Exception e) {
                            //
                        }
                    }
                });
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        doAsynchronousTask.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        data.getTimer().cancel();
        data.setTimer(new Timer());
        createTask(data.getHandler());
        data.getTimer().schedule(doAsynchronousTask, 0, SettingsActivity.DEFAULT_FETCHERS_INTERVAL);
    }
}
