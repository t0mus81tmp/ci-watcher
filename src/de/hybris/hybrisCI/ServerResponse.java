package de.hybris.hybrisCI;

public class ServerResponse {
    private final String response;
    private final int code;

    public ServerResponse(String response, int code) {
        this.response = response;
        this.code = code;
    }

    public String getResponse() {
        return response;
    }

    public int getCode() {
        return code;
    }
}