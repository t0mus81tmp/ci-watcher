package de.hybris.hybrisCI;

class StatusCodes {
    public static final int OK = 200;
    public static final int NOT_FOUND = 404;
}
