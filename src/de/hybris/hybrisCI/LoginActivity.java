package de.hybris.hybrisCI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends BaseActivity {

    public static final String PASS = "pass";
    public static final String USER = "user";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getSupportActionBar().hide();
        data = (LoginData) getLastCustomNonConfigurationInstance();
        if(data==null){
            data = new LoginData();
            SharedPreferences preferences = FavouritesUtil.getPreferences(LoginActivity.this);
            ((LoginData)data).setUser(preferences.getString(USER, ""));
            ((LoginData)data).setPass(preferences.getString(PASS, ""));
            data.setProgressDialog(null);
            data.setErrorDialog(null);
        }else{
            if(data.isErrorDialogDisplayed()){
                data.setErrorDialog(DialogsUtil.createErrorDialog(data.getErrorCode(), LoginActivity.this));
                data.getErrorDialog().show();
            }else{
                if(data.isProgressDialogDisplayed()){
                    data.setProgressDialog(ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading)));
                }
            }
        }
        ((EditText) findViewById(R.id.user)).setText(((LoginData)data).getUser());
        ((EditText) findViewById(R.id.pass)).setText(((LoginData)data).getPass());
    }

    public void login(@SuppressWarnings("UnusedParameters") View view) {
        SharedPreferences.Editor editor = FavouritesUtil.getPreferencesEditor(LoginActivity.this);
        editor.putString(USER, ((EditText) findViewById(R.id.user)).getText().toString());
        editor.putString(PASS, ((EditText) findViewById(R.id.pass)).getText().toString());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        } else {
            editor.commit();
        }
        if (isOnline()) {
            data.setProgressDialog(ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.loading)));
            JSONUserFetcher fetcher = new JSONUserFetcher(SettingsActivity.HTTPS_BAMBOO_HYBRIS_COM_REST_API_LATEST + "/currentUser.json", SettingsActivity.AUTH_TYPE, ((EditText) findViewById(R.id.user)).getText().toString(), ((EditText) findViewById(R.id.pass)).getText().toString(), String.valueOf(SettingsActivity.DEFAULT_START_INDEX), String.valueOf(SettingsActivity.SINGLE_RESULT), "");
            ((LoginData)data).setFetcher(fetcher);
            fetcher.execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
        return true;
    }

    private class JSONUserFetcher extends JSONFetcher {

        public JSONUserFetcher(String... params) {
            super(params);
        }

        @Override
        protected void onPostExecute(ServerResponse serverResponse) {
            if (data.getProgressDialog() != null) {
                String response = serverResponse.getResponse();
                if (response != null) {
                    DialogsUtil.dismissDialog(data.getProgressDialog());
                    data.setProgressDialog(null);
                    Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
                    intent.putExtra(USER, ((EditText) findViewById(R.id.user)).getText().toString());
                    intent.putExtra(PASS, ((EditText) findViewById(R.id.pass)).getText().toString());
                    startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    DialogsUtil.dismissDialog(data.getProgressDialog());
                    data.setProgressDialog(null);
                    AlertDialog errorDialog = DialogsUtil.createErrorDialog(serverResponse.getCode(), LoginActivity.this);
                    data.setErrorCode(serverResponse.getCode());
                    data.setErrorDialog(errorDialog);
                    errorDialog.show();
                }
            }
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        data.setProgressDialogDisplayed(data.getProgressDialog() != null && data.getProgressDialog().isShowing());
        data.setErrorDialogDisplayed(data.getErrorDialog() != null && data.getErrorDialog().isShowing());
        DialogsUtil.dismissDialog(data.getProgressDialog());
        DialogsUtil.dismissDialog(data.getErrorDialog());
        return data;
    }
}
